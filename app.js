let express = require('express');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let devicesRouter = require('./routes/devices');

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/', indexRouter);
app.use('/devices', devicesRouter);

module.exports = app;
