var express = require('express');
var router = express.Router();
var devicesController = require('./../controllers/deviceController')

var devicesResponse = {devices : [{name:'luz 1'},{name: 'luz 2'},{name: 'luz 3'}]};

router.get('/', function(req, res, next){
    res.json(devicesResponse);
});

router.get('/:deviceId/:status', devicesController.setStatus);

module.exports = router;